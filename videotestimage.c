#define XRES 1920
#define YRES 1080

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

typedef unsigned char  byte;
typedef unsigned short word;
  
typedef struct TGA_HEADER
{
	byte num_ID_chars;
	byte col_map_pres;
	byte image_type;
	byte col_map_start_lo;
	byte col_map_start_hi;
	byte entry_byte;
	byte col_map_len_lo;
	byte col_map_len_hi;
	word x_origin;
	word y_origin;
	word width;
	word height;
	byte num_bpp;
	byte image_descr;
} TGA_HEADER;


// ffmpeg -loop 1 -i image.tga -c:v libkvazaar -t 15 -pix_fmt yuv420p out.mp4
// ffmpeg -loop 1 -r 30 -i image.tga -t 00:00:02 -vcodec libkvazaar -kvazaar-params "input-res=auto,bitrate=10000000,preset=ultrafast,tiles=3x2" -an output.mp4
// ffmpeg -loop 1 -r 30 -i image.tga -t 00:00:02 -vcodec libkvazaar -kvazaar-params "input-res=auto,bitrate=10000000,preset=ultrafast,tiles-width-split=128:256:384:512:640:768:896:1024,tiles-height-split=128:256:384:512:640:768:896:1024" -an output.mp4

// ffmpeg -i "D:\Media Backup\Movies HD (m4v)\Blade Runner 2049.m4v" -vcodec libkvazaar -kvazaar-params "input-res=auto,bitrate=10000000,preset=ultrafast,tiles-width-split=128:256:384:512:640:768:896:1024,tiles-height-split=128:256:384:512:640:768:896:1024" output.mp4


int arrayContains(word array[], word value, int size)
{
	int i;
	
	for (i = 0; i < size; i++)
	{
		if (array[i] == value) return i;
	}
	
	return -1;
}

void screen( char *picname)
{
	int         x,y;
	word        xres,yres;   
	byte       *buffer, *ptrstore;     
	TGA_HEADER  header;   
	FILE       *outptr = fopen(picname,"wb");
	byte 		r,g,b;
	
	word xtiles[] = {128,256,384,512,640,768,896,1024};
	word ytiles[] = {128,256,384,512,640,768,896,1024};
			
	ptrstore = buffer =  malloc(XRES * YRES * 3);

	int xindex = -1;
	int yindex = -1;
	int xstr = 0;
	int ystr = 0;
	
	r = g = b = 0;

	for (yres = 0; yres < YRES; yres++)
	{ 
		for (xres = 0; xres < XRES; xres++)
		{    			
			if ((xindex = arrayContains (xtiles, xres, sizeof(xtiles)/ sizeof(word) )) != -1)
			{
				xstr = xindex;
			}
			
			if ((yindex = arrayContains (ytiles, yres, sizeof(ytiles)/ sizeof(word))) != -1)
			{
				ystr = yindex;
			}
			
			if (xindex == -1 && yindex == -1)
			{
				r = 32 * xstr;
				g = 32 * ystr;
				b = 255 - (32 * ystr);
			}
			else
			{
				if ((xres & 1) || (yres & 1))
					r = g = b = 0;
				else
					r = g = b = 255;
			}
			
			*(ptrstore++) = (byte)b;    
			*(ptrstore++) = (byte)g;
			*(ptrstore++) = (byte)r;
		}   
		
		xstr = 0;
	}

	header.num_ID_chars     = 0;
	header.col_map_pres     = 0;
	header.image_type       = 2;
	header.col_map_start_lo = 0;
	header.col_map_start_hi = 0;
	header.entry_byte       = 0;
	header.col_map_len_lo   = 0;
	header.col_map_len_hi   = 0;
	header.x_origin         = 0;
	header.y_origin         = 0;
	header.width            = XRES;
	header.height           = YRES;
	header.num_bpp          = 24;
	header.image_descr      = 0;

	fwrite(&header,sizeof(TGA_HEADER),1,outptr);
	fwrite(buffer,XRES * YRES,3,outptr);

	printf("DONE Picture '%s'\n",picname); 

	fclose(outptr);
	free(buffer);
}

void main()
{
	screen("image.tga");
}
